#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""This is a program to run the backup jobs individually through arguments passed to it.

It's done this way so one can perform backup jobs that need to run at different frequencies.
"""

import sys

from AppData.BackupToolApp.cli import main

if __name__ == "__main__":
    sys.exit(main())
