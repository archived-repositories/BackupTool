#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Main backup job classes.
"""

import os
import sys

from subprocess import PIPE
from subprocess import Popen
from subprocess import STDOUT
from threading import Thread

from . import app_utils
from .python_utils import file_utils
from .python_utils import misc_utils
from .python_utils import shell_utils


class BackupJobs():
    """Backup jobs "processor" class.

    Attributes
    ----------
    jobs : list
        Initialized backup jobs storage.
    """

    def __init__(self, backup_jobs, logger):
        """
        Parameters
        ----------
        backup_jobs : list
            The list of "backup jobs" to perform.
        logger : object
            See <class :any:`LogSystem`>.

        Raises
        ------
        err
            Could raise if the passed backup jobs are malformed.
        """
        super().__init__()
        self.jobs = []

        global_settings = backup_jobs.get("global_settings", dict())

        try:
            for job_settings in backup_jobs["jobs"]:
                self.jobs.append(Backup(
                    job_settings=job_settings,
                    global_settings=global_settings,
                    logger=logger
                ))
        except Exception as err:
            raise err

    def threaded_job(self, job):
        """Threaded job.

        Parameters
        ----------
        job : dict
            Initialized job to start a thread.

        Returns
        -------
        threading.Thread
            Return the thread to be joined.
        """
        thread = Thread(target=job.run)
        thread.start()
        return thread

    def run(self):
        """Run.
        """
        threads = []

        for job in self.jobs:
            threads.append(self.threaded_job(job))

            for thread in threads:
                if thread is not None:
                    thread.join()


class Backup():
    """Main backup job class.

    Attributes
    ----------
    backup_elapsed_time : str
        Backup job duration.
    backup_end_time : str
        The time at which the backup job ended.
    backup_errors : int
        Number of errors produced during a backup job.
    backup_items : list
        The list of backup items (Not validated).
    backup_list : list
        The list of backup items (Validated).
    backup_name : str
        An optional name for the backup job to be displayed on lg messages, etc.
    backup_start_time : str
        The time at which the backup job started.
    backup_type : str
        The type of backup to perform.
    desktop_notification : bool
        Whether or not to perform a desktop notification when a backup job ends.
    destination : str
        The destination file/folder for the backed up items.
    ignore_symlinks : bool
        Wheter the backup job will ignore to back up symlinks.
    ignored_patterns : list
        A list of file name patterns to be ignored by the copy functions.
    items_root_folder : str
        An optional path to a folder that will be used to "generate" the full path of the items
        that will be backed up.
    logger : object
        See <class :any:`LogSystem`>.
    mail_notification : bool
        Whether or not to send an email notification when a backup job ends.
    mail_settings : dict
        The settings that will be used by the :any:`mail_system.MailSystem`.
    sound_notification : bool
        Whether or not to perform a sound notification when a backup job ends.
    """

    def __init__(self, job_settings={}, global_settings={}, logger=None):
        """
        Parameters
        ----------
        job_settings : dict, optional
            The actual backup job to be processed.
        global_settings : dict, optional
            Global settings to attach to the backup job.
        logger : object
            See <class :any:`LogSystem`>.

        Raises
        ------
        RuntimeError
            Could raise if "backup_type" option is not present among the backup job settings.
        """
        self.logger = logger
        self.backup_elapsed_time = ""
        self.backup_start_time = misc_utils.get_date_time()
        self.backup_list = []
        self.backup_errors = 0

        self.backup_items = job_settings.get("items", [])
        self.backup_type = job_settings.get("backup_type", None)
        self.backup_name = job_settings.get("backup_name", str(self.backup_type))
        self.sound_notification = global_settings.get("sound_notification", False)
        self.desktop_notification = global_settings.get("desktop_notification", False)
        self.mail_notification = global_settings.get("mail_notification", False)
        self.ignore_symlinks = global_settings.get("ignore_symlinks", False)

        if self.mail_notification:
            self.mail_settings = global_settings.get("mail_settings", dict())

        if job_settings.get("ignore_symlinks", False):
            self.ignore_symlinks = True

        self.items_root_folder = job_settings.get("items_root_folder", None)
        prefix = job_settings.get("destination_prefix", "")

        if prefix is not "":
            prefix += "-"

        date_for_filename = misc_utils.micro_to_milli(misc_utils.get_date_time("filename"))

        if self.backup_type is "stacked":
            self.destination = os.path.join(job_settings["destination"],
                                            prefix + date_for_filename)
        elif self.backup_type is "incremental":
            self.destination = job_settings["destination"]
        elif self.backup_type is "compressed":
            self.destination = os.path.join(job_settings["destination"],
                                            prefix + date_for_filename + ".tar.xz")
        else:
            raise RuntimeError(
                "Non optional setting <backup_type> missing or with wrong value. Value found: <%s>" % self.backup_type) from None

        # Work with sets at first to avoid repeated items.
        globally_ignored_patterns = set(global_settings.get("ignored_patterns", []))
        job_ignored_patterns = set(global_settings.get("ignored_patterns", []))

        # There shouldn't be a need to convert it back into a list, but lets do it
        # to avoid surprises. Furthermore, the "ignore_patterns" function of the
        # "shutil" module expects a list.
        self.ignored_patterns = list(globally_ignored_patterns.union(job_ignored_patterns))

    def validate_list(self):
        """Validate the list of items to be backed up.

        - If it's empty, get out.
        - If an item exists and is not a symlink, keep it on the list. Otherwise, ignore it.
        - Display the list of ignored items all at once at the end of the validation process.
        """
        if not self.backup_items:
            # Using the logger and sys.exit() because the freaking exceptions.CustomRuntimeError
            # class doesn't work in here!!! (/&···/&·/&$$/!!!)
            # It works as expected in cli.py:main, WHY THE HELL DOESN'T WORK HERE?!?!?!?!!
            msg = "Empty backup_items list! Can't proceed! Leaving..."
            self.logger.error(msg)
            sys.exit(0)

        ignored_items = []

        for item in self.backup_items:
            item_path = os.path.abspath(
                os.path.join(self.items_root_folder,
                             item) if self.items_root_folder is not None else item)

            if os.path.exists(item_path) and not os.path.islink(item_path):
                self.backup_list.append(item_path)
            else:
                self.increment_backup_errors_count()
                # Just store the ignored items.
                ignored_items.append(item_path)

        # If there are ignored items, log them all at once.
        if ignored_items:
            self.logger.warning(
                "Invalid items. They weren't added to the backup list:\n%s" % "\n".join(ignored_items))

    def threaded_item(self, source, destination, is_dir=False):
        """"Thread" the :any:`Backup.process_item` usage.

        Parameters
        ----------
        source : str
            Source path.
        destination : str
            Destination path.
        is_dir : bool
            Whether the item is a directory or not.

        Returns
        -------
        threading.Thread
            A "thread" to be joined.
        """
        thread = Thread(target=self.process_item,
                        args=(source, destination, is_dir))
        thread.start()
        return thread

    def process_item(self, source, destination, is_dir):
        """Process individual items on the backup items list.

        Parameters
        ----------
        source : str
            Source path.
        destination : str
            Destination path.
        is_dir : bool
            Whether the item is a directory or not.
        """
        if is_dir:
            try:
                # custom_copytree can handle non existent destination folders.
                file_utils.custom_copytree(source, destination, symlinks=not self.ignore_symlinks,
                                           ignored_patterns=self.ignored_patterns,
                                           logger=self.logger)
            except Exception as e:
                self.increment_backup_errors_count()
                self.logger.error("Error processing directory <%s>: <%s>" % (source, e))
        else:
            try:
                # copy2 can't candle non existent destination folders, so create it
                # before attempting the file copy operation.
                container_folder = os.path.dirname(destination)

                if not os.path.exists(container_folder):
                    os.makedirs(container_folder, exist_ok=True)

                file_utils.custom_copy2(source, destination, self.logger)
            except Exception as e:
                self.increment_backup_errors_count()
                self.logger.error("Error processing file <%s>: <%s>" % (source, e))

    def process_list(self):
        """Process the list of items depending on the backup job type.
        """
        threads = []
        default_dest = os.path.abspath(os.path.join(self.destination))

        if self.backup_type is "stacked" or self.backup_type is "incremental":
            for item in self.backup_list:
                try:
                    source = os.path.abspath(item)

                    if self.items_root_folder is not None:
                        destination = os.path.join(
                            default_dest, os.path.relpath(item, self.items_root_folder))
                    else:
                        destination = os.path.join(default_dest, item)

                    if os.path.isdir(item):
                        self.logger.debug("Processing directory: %s" % item)
                        threads.append(self.threaded_item(source, destination, is_dir=True))
                    else:
                        self.logger.debug("Processing file: %s" % item)
                        threads.append(self.threaded_item(source, destination, is_dir=False))

                    for thread in threads:
                        if thread is not None:
                            thread.join()
                except Exception as e:
                    self.increment_backup_errors_count()
                    self.logger.error(
                        "Failed to process item <%s> in <backup_list>: %a" % (item, e))
                    continue

            self.notify()
        elif self.backup_type is "compressed":
            t = Thread(target=self.do_compression)
            t.start()

            threads.append(t)

            for thread in threads:
                if thread is not None:
                    thread.join()

    def do_compression(self):
        """Do the compressed backup job.
        """
        import tempfile

        files_list = app_utils.FilesListForTar(
            self.backup_list, self.ignored_patterns).get_full_list_of_files()
        full_list_of_files = "\n".join(files_list)
        env = {**os.environ, "XZ_OPT": "-7"}
        destination_dir = os.path.dirname(self.destination)

        if not os.path.exists(destination_dir):
            os.makedirs(destination_dir)

        with tempfile.NamedTemporaryFile(prefix="compressed-backup-files-list-", delete=False) as tmp_file:
            tmp_file.write(str.encode(full_list_of_files))
            tmp_file.seek(0)

            p = Popen([
                "tar",
                "--keep-directory-symlink",
                "--create",
                "--preserve-permissions",
                "--xz",
                "--file",
                self.destination,
                "--files-from=" + tmp_file.name,
                "--totals",
                "--record-size=1M",
                "--checkpoint=50",
                '--checkpoint-action=echo="%T"'
            ],
                stdout=PIPE,
                stderr=STDOUT,
                env=env)

            while True:
                # Without decoding output, the loop will run forever. ¬¬
                output = p.stdout.read(1).decode("utf-8")

                if output == "" and p.poll() is not None:
                    break

                if output:
                    sys.stdout.write(output)
                    sys.stdout.flush()

        print("")
        self.notify()

    def notify(self):
        """Notify that the backup job ended.
        """
        self.backup_end_time = misc_utils.get_date_time()
        self.backup_elapsed_time = misc_utils.get_time_diff(
            self.backup_start_time, self.backup_end_time)

        n_errors = self.get_backup_errors_count()
        message = app_utils.REPORT_TEMPLATE.format(
            backup_name=self.backup_name,
            backup_type=self.backup_type,
            n_errors=n_errors,
            started=misc_utils.micro_to_milli(self.backup_start_time),
            finished=misc_utils.micro_to_milli(self.backup_end_time),
            elapsed=self.backup_elapsed_time,
        )

        # TODO: Investigate if this is recommended.
        getattr(self.logger, "warning" if n_errors != 0 else "success")(message)

        if self.sound_notification:
            app_utils.play_sound(os.path.join(
                os.getcwd(), "AppData", "data", "sounds", "ding.wav"))

        if self.desktop_notification:
            app_utils.notify_send(self.backup_name, message, logger=self.logger)

        if self.mail_notification:
            from . import mail_system

            mail = mail_system.MailSystem(self.logger, self.mail_settings)
            subject = self.mail_settings.get("mail_subject", "Backup Tool Report")

            mail_body = message + "\n" + self.mail_settings.get("mail_body", "")

            mail.send(subject, mail_body)

    def increment_backup_errors_count(self):
        """Set backup errors.
        """
        self.backup_errors += 1

    def get_backup_errors_count(self):
        """Get backup errors.

        Returns
        -------
        int
            The number of errors produced during the backup job.
        """
        return self.backup_errors

    def run(self):
        """Start the backup job.
        """
        self.logger.info(shell_utils.get_cli_separator(), date=False)
        self.logger.info("Starting <%s> backup job..." % self.backup_name)
        self.validate_list()
        self.process_list()


if __name__ == "__main__":
    pass
