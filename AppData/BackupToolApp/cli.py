#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Main command line application.

Attributes
----------
docopt_doc : str
    Used to store/define the docstring that will be passed to docopt as the "doc" argument.
root_folder : str
    The main folder containing the application. All commands must be executed from this location
    without exceptions.
"""

import os
import sys

from collections import OrderedDict
from runpy import run_path
from threading import Thread

from . import app_utils
from .__init__ import __appdescription__
from .__init__ import __appname__
from .__init__ import __status__
from .__init__ import __version__
from .backup import BackupJobs
from .python_utils import cli_utils


root_folder = os.path.realpath(os.path.abspath(os.path.join(
    os.path.normpath(os.getcwd()))))


docopt_doc = """{appname} {version} ({status})

{appdescription}

Usage:
    app.py (-h | --help | --manual | --version)
    app.py backup (-j <name> | --jobs-rel=<name>
                  | -J <path> | --jobs-full=<path>)
                  [-j <name>... | --jobs-rel=<name>...]
                  [-J <path>... | --jobs-full=<path>...]
                  [-g <name> | --globals-rel=<name>
                  | -G <path> | --globals-full=<path>]
    app.py generate (jobs | globals | system_executable)
    app.py check_notification (sound | desktop |
                              mail (-g <name> | --globals-rel=<name>
                                   | -G <path> | --globals-full=<path>))

Options:

-h, --help
    Show this screen.

--manual
    Show this application manual page.

--version
    Show application version.

-j, --jobs-rel=<name>
    File name of the file containing the backup jobs. Should be the name of a
    file stored in UserData/backup_jobs/<name>.py. Extension omitted.

-J, --jobs-full=<path>
    Similar to `--jobs-rel`, but it should specify the full path to a file
    containing the backup jobs.

-g, --globals-rel=<name>
    File name of the file containing the global settings that all the backup
    jobs will use. Should be the name of a file stored in
    UserData/backup_jobs/<name>.py. Extension omitted.

-G, --globals-full=<path>
    Similar to `--globals-rel`, but it should specify the full path to a file
    containing the global settings that all the backup jobs will use.

Sub-commands for the `generate` command:
    jobs                 Generate an empty backup jobs file from a template.
    globals              Generate an empty general settings from a template.
    system_executable    Create an executable for this application on the system
                         PATH to be able to run it from anywhere.

Sub-commands for the `check_notification` command:
    sound                Check sound notification.
    desktop              Check desktop notification.
    mail                 Check mail notification. Requires to have a global
                         settings file with email data configured.

Examples:
    app.py --jobs-rel=file_name --jobs-full=file_path --globals-rel=file_name
    app.py --jobs-rel=file_name1 --jobs-rel=file_name2 --globals-full=file_path

""".format(appname=__appname__,
           appdescription=__appdescription__,
           version=__version__,
           status=__status__)


class CommandLineInterface(cli_utils.CommandLineInterfaceSuper):
    """Command line interface.

    It handles the arguments parsed by the docopt module.

    Attributes
    ----------
    a : dict
        Where docopt_args is stored.
    action : method
        Set the method that will be executed when calling CommandLineTool.run().
    global_settings : list
        Global settings.
    jobs : list
        Storage for the backup jobs that will be performed.
    """
    action = None
    global_settings = None

    def __init__(self, docopt_args):
        """
        Parameters
        ----------
        docopt_args : dict
            The dictionary of arguments as returned by docopt parser.

        Raises
        ------
        Exception
            Error reading globals settings file.
        """
        self.a = docopt_args
        self._cli_header_blacklist = [self.a["--manual"]]

        super().__init__(__appname__, "UserData/logs")

        try:
            if self.a.get("--globals-rel", None):
                self.global_settings = run_path(os.path.join(root_folder,
                                                             "UserData",
                                                             "backup_jobs",
                                                             self.a["--globals-rel"] + ".py"
                                                             )
                                                )["global_settings"]
            elif self.a.get("--globals-full", None):
                self.global_settings = run_path(os.path.abspath(
                    self.a["--globals-full"]))["global_settings"]
        except Exception as err:
            raise Exception(err)

        if self.a["--manual"]:
            self.action = self.display_manual_page
        elif self.a["check_notification"]:
            if self.a["sound"]:
                self.action = self.check_sound_notification

            if self.a["desktop"]:
                self.action = self.check_desktop_notification

            if self.a["mail"]:
                self.logger.info("Checking mail notification...")
                self.action = self.check_mail_notification
        elif self.a["generate"]:
            if self.a["jobs"]:
                self.logger.info("Generating backup jobs file...")
                self.action = self.generate_backup_jobs_template

            if self.a["globals"]:
                self.logger.info("Generating global settings file...")
                self.action = self.generate_global_settings_template

            if self.a["system_executable"]:
                self.logger.info("System executable generation...")
                self.action = self.system_executable_generation
        elif self.a["backup"] and (self.a["--jobs-rel"] or self.a["--jobs-full"]):
            self.action = self.do_backups

            self.jobs = []

            if self.a["--jobs-rel"]:
                self.process_jobs("rel",
                                  list(OrderedDict.fromkeys(self.a["--jobs-rel"])))

            if self.a["--jobs-full"]:
                self.process_jobs("full",
                                  list(OrderedDict.fromkeys(self.a["--jobs-full"])))

    def process_jobs(self, type, jobs):
        """Process jobs.

        Process the full path or file name arguments that contain the backup jobs themselves.
        Then they are stored into self.jobs for later execution.

        Parameters
        ----------
        type : str
            There are two types.

            - rel (relative): This is actually the name of a file (without extension) located \
            inside UserData/backup_jobs folder.
            - full (full path): This is a full path to a file (with extension) located anywhere on \
            the file system.

        jobs : list
            The list of files to be processed as backup jobs. Passed by the `--jobs-rel` and/or
            `--jobs-full` arguments.
        """
        for j in jobs:
            try:
                if type == "rel":
                    job_data = run_path(os.path.join(root_folder, "UserData",
                                                     "backup_jobs", j + ".py"))["backup_jobs"]
                elif type == "full":
                    job_data = run_path(os.path.abspath(j))["backup_jobs"]

                if self.global_settings is not None:
                    job_data["global_settings"] = self.global_settings

                self.jobs.append(job_data)
            except Exception as err:
                self.logger.error(j)
                self.logger.error(err)

    def run(self):
        """Execute the assigned action stored in self.action if any.
        """
        if self.action is not None:
            self.action()
            sys.exit(0)

    def do_backups(self):
        """Perform the backups jobs through joined threads.
        """
        threads = []

        if self.jobs is not []:
            for job in self.jobs:
                j = BackupJobs(backup_jobs=job, logger=self.logger)
                t = Thread(target=j.run)
                t.start()
                threads.append(t)

                for thread in threads:
                    if thread is not None:
                        thread.join()
        else:  # This may never be displayed.
            self.logger.warning("There are no backup jobs to perform!!!")

    def generate_backup_jobs_template(self):
        """See :any:`app_utils.generate_from_app_template`
        """
        app_utils.generate_from_app_template(
            "backup_jobs", "py", "backup_jobs", self.logger)

    def generate_global_settings_template(self):
        """See :any:`app_utils.generate_from_app_template`
        """
        app_utils.generate_from_app_template(
            "global_settings", "py", "backup_jobs", self.logger)

    def system_executable_generation(self):
        """See :any:`cli_utils.CommandLineInterfaceSuper._system_executable_generation`.
        """
        self._system_executable_generation(
            exec_name="backup-tool-cli",
            app_root_folder=root_folder,
            sys_exec_template_path=os.path.join(
                root_folder, "AppData", "data", "templates", "system_executable"),
            bash_completions_template_path=os.path.join(
                root_folder, "AppData", "data", "templates", "bash_completions.bash"),
            logger=self.logger
        )

    def check_sound_notification(self):
        """See :any:`app_utils.check_notification`
        """
        app_utils.check_notification("sound", logger=self.logger)

    def check_desktop_notification(self):
        """See :any:`app_utils.check_notification`
        """
        app_utils.check_notification("desktop", logger=self.logger)

    def check_mail_notification(self):
        """See :any:`app_utils.check_notification`
        """
        app_utils.check_notification("mail", logger=self.logger)

    def display_manual_page(self):
        """See :any:`cli_utils.CommandLineInterfaceSuper._display_manual_page`.
        """
        self._display_manual_page(os.path.join(root_folder, "AppData", "data", "man", "app.py.1"))


def main():
    """Initialize command line interface.
    """
    cli_utils.run_cli(flag_file=".backup-tool.flag",
                      docopt_doc=docopt_doc,
                      app_name=__appname__,
                      app_version=__version__,
                      app_status=__status__,
                      cli_class=CommandLineInterface)


if __name__ == "__main__":
    pass
