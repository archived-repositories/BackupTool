#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Module with utility functions and classes.

Attributes
----------
REPORT_TEMPLATE : str
    Template used by a finalized task report.
root_folder : str
    The main folder containing the application. All commands must be executed from this location
    without exceptions.
TEST_NOTIFICATION_BODY : str
    String to be used when testing the desktop notifications to be used as its body.
"""

import os


from shutil import ignore_patterns

from .python_utils import cmd_utils
from .python_utils import exceptions
from .python_utils import prompts

root_folder = os.path.realpath(os.path.abspath(os.path.join(
    os.path.normpath(os.getcwd()))))


REPORT_TEMPLATE = """Backup job finished
    Backup Name:        {backup_name}
    Backup Type:        {backup_type}
    Number of Errors:   {n_errors}
    Started at:         {started}
    Finished at:        {finished}
    Elapsed Time:       {elapsed}
"""


TEST_NOTIFICATION_BODY = """Test notification body
<b>Icon:</b> {icon}
<b>Urgency:</b> {urgency}
"""


class FilesListForTar():
    """Create a files list.

    Create a comprehensive list of files and folders based on passed "base list" and filter the
    list using shutil.ignore_patterns.

    This list of files is used to be passed to `tar` as an argument for its `--files-from` option. I
    deemed this infinitely more simple and less prone to errors than using `tar` inside a loop and
    using `tar`'s arguments to filter the files.

    Attributes
    ----------
    backup_list : list
        The list of files/folders already validated by :any:`backup.Backup.validate_list`
    full_list_of_files : list
        The already processed backup_list.
    ignored_patterns : list
        A list of file patters to not include into full_list_of_files.
    """

    def __init__(self, backup_list, ignored_patterns):
        """Initialize.

        Parameters
        ----------
        backup_list : list
            The list of files/folders already validated by :any:`backup.Backup.validate_list`
        ignored_patterns : list
            A list of file patters to not include into full_list_of_files.
        """
        super().__init__()
        self.backup_list = backup_list
        self.ignored_patterns = ignored_patterns
        self.full_list_of_files = []

        self.populate_list()

    def populate_list(self):
        """Populate the full_list_of_files.

        If an item in "backup_list" is the path to a file or a symlink, append it to
        "full_list_of_files". If it's a folder, recursively process the folder.

        """
        for item in self.backup_list:
            if os.path.isdir(item) and not os.path.islink(item):
                self.process_directory(item)
            else:
                self.full_list_of_files.append(item)

    def process_directory(self, directory):
        """Process directory.

        Parameters
        ----------
        directory : str
            The directory to process.
        """
        names = os.listdir(directory)

        if self.ignored_patterns is not None:
            ignored_names = ignore_patterns(*self.ignored_patterns)(directory, names)
        else:
            ignored_names = set()

        errors = []

        for name in names:
            if name in ignored_names:
                continue

            srcname = os.path.join(directory, name)

            try:
                if os.path.isdir(srcname) and not os.path.islink(srcname):
                    self.process_directory(srcname)
                else:
                    self.full_list_of_files.append(srcname)
            except exceptions.Error as err:
                errors.extend(err.args[0])
            except OSError as why:
                errors.append((srcname, str(why)))

    def get_full_list_of_files(self):
        """
        Returns
        -------
        list
            The full list of files.
        """
        return self.full_list_of_files


def play_sound(soundfile, logger=None):
    """Play a sound file using `aplay` command.

    Parameters
    ----------
    soundfile : str
        The path to a sound file.
    logger : object
        See <class :any:`LogSystem`>.
    """
    try:
        cmd_utils.popen(["play", "-V0", "--no-show-progress", "--type=.wav", soundfile],
                        cwd=root_folder, logger=logger)
    except Exception as err:
        try:
            cmd_utils.popen(["aplay", "--quiet", "--file-type=wav", soundfile],
                            cwd=root_folder, logger=logger)
        except Exception:
            if logger is not None:
                logger.error(err)
            else:
                pass


def notify_send(title, body, urgency="normal", icon="dialog-info", logger=None):
    """Send a desktop notification using `notify-send` command.

    Parameters
    ----------
    title : str
        Notification title.
    body : str
        notification body.
    urgency : str, optional
        Notification priority.
    icon : str, optional
        Notification ison.
    logger : object
        See <class :any:`LogSystem`>.
    """
    try:
        cmd_utils.popen(["notify-send", "--urgency=%s" % urgency, "--category=transfer",
                         "--icon=%s" % icon, "--hint=int:resident:1", title, body],
                        cwd=root_folder, logger=logger)
    except Exception as err:
        if logger is not None:
            logger.error(err)
        else:
            pass


def check_notification(type, globals=None, logger=None):
    """Notifications checker.

    Check if the three types of notifications used by this application will work correctly.

    Parameters
    ----------
    type : str
        Which type of notification to test ("sound", "desktop" or "mail").
    globals : None, optional
        Not implemented yet.
    logger : object
        See <class :any:`LogSystem`>.

    Raises
    ------
    NotImplemented
        "mail" notification checker not implemented yet.
    """
    if type == "sound":
        logger.info("Checking sound notification...")
        play_sound(os.path.join(root_folder, "AppData", "data", "sounds", "ding.wav"), logger)
    elif type == "desktop":
        logger.info("Checking desktop notification...")
        notify_send("Test notification title", TEST_NOTIFICATION_BODY.format(
            icon="dialog-info",
            urgency="low",
        ), icon="dialog-info", urgency="low", logger=logger)

        notify_send("Test notification title", TEST_NOTIFICATION_BODY.format(
            icon="dialog-warning",
            urgency="normal",
        ), icon="dialog-warning", urgency="normal", logger=logger)
    elif type == "mail":
        logger.info("Checking desktop notification...")
        raise NotImplemented("Not implemented yet...")


def generate_from_app_template(filename, extension, destination, logger):
    """Generate file from template.

    Parameters
    ----------
    filename : str
        File name of the file to generate.
    extension : str
        Extension name of the template file.
    destination : str
        Path to where the generated file will be saved.
    logger : object
        See <class :any:`LogSystem`>.
    """
    from .python_utils import template_utils

    d = {"name": ""}
    prompts.do_prompt(d, "name", "Enter a file name", filename)

    src = os.path.join(root_folder, "AppData", "data", "templates",
                       "%s_template.%s" % (filename, extension))
    dst = os.path.join(root_folder, "UserData", destination,
                       "%s.%s" % (d["name"], extension))

    template_utils.generate_from_template(src, dst, logger=logger)


if __name__ == "__main__":
    pass
