#!/usr/bin/python3
# -*- coding: utf-8 -*-

__status__ = "Stable"
__appname__ = "Backup Tool"
__appdescription__ = "A CLI utility to backup files on GNU/Linux."
__version__ = "2018-09-15 01:19:13.334"


if __name__ == "__main__":
    pass
