#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Only imported so one can use it to generate the HOME folder path to be used by
# the "items_root_folder" option on this example.
from os import path

# This variable should always be named "backup_jobs".
backup_jobs = {
    # "global_settings" could be defined here. Or it could be defined in a
    # separated file to be passed to each individual backup job on runtime.
    # "global_settings": {...},
    "jobs": [
        {
            # backup_type: (string) (Mandatory)
            #
            # Possible values: "stacked", "compressed" or "incremental".
            #
            # stacked: Each execution of this "backup job" will generate a
            # complete new copy of the backed up files/folders into a
            # "destination" folder.
            #
            # compressed: Same as "stacked" backups, but the backed up
            # files/folders will be saved into a compressed archive inside
            # the "destination" folder.
            #
            # incremental: Each execution of this "backup job" will update
            # the files/folders that were backed up in the first run.
            # New files/folders found on the source will be added, but old
            # files that were backed up on previous executions will not
            # be deleted.
            "backup_type": "stacked",

            # backup_name: (string) (Optional)
            #
            # A descriptive name for the backup job. Used mostly in logs
            # and emails.
            "backup_name": "My home backup job",

            # destination: (string) (Mandatory)
            #
            # stacked: In a "stacked" back up job, with each execution of
            # the job, a new sub-folder named after the current date
            # (plus "destination_prefix" if set) will be created inside
            # "destination" that will contain the backed up files/folders.
            #
            # compressed: Same as in a "stacked" back up job, but instead
            # of backing up into a sub-folder inside "destination", a
            # compressed archive will be created. The name convention for
            # the generated archive will also be the same as in a
            # "stacked" back up job (<destination_prefix>-<date>).
            #
            # incremental: In an "incremental" backup "job", the
            # files/folders will be always copied into "destination",
            # updating existent files only if the source files are newer.
            # Files that were removed from the source will not be removed
            # from the "destination".
            "destination": "/full/path/to/backups/location",

            # destination_prefix: (string) (Optional)
            #
            # A string that will be added as a prefix to the "stacked"
            # backups sub-folder name or to the "compressed" backups
            # compressed file name. "incremental" backups will ignore this key.
            "destination_prefix": "MyHome",

            # items_root_folder: (string) (Optional)
            #
            # The "items_root_folder" folder has two uses.
            # 1. It's used to generate the full path to the source
            #    files/folders. So one doesn't have to use full paths while
            #    specifying the "items", making it less cumbersome.
            # 2. The "items_root_folder" path will be removed from the
            #    backed up files/folders path when they are copied into
            #    "destination". This keeps the "destination" paths not too
            #    long and it's also easy to navigate because it will be a
            #    "mirror" of the "items_root_folder".
            "items_root_folder": path.expanduser("~"),

            # ignored_patterns: (list) (Optional)
            #
            # A list of file name patterns to be ignored by the copy functions.
            # This list will be merged with the "globally_ignored_patterns" list.
            "ignored_patterns": ["*.ext3", "*.ext4"],

            # items: (list) (Mandatory)
            #
            # The list of files/folders to back up. Could be the absolute
            # path to the files/folders or could be paths relative to
            # "items_root_folder". Use one method or the other
            # (relative paths OR absolute paths), NOT BOTH.
            "items": [
                "/path/to/file/relative/items_root_folder",
                "/path/to/folder/relative/items_root_folder"
            ]
        }
    ]
}
