#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Define global settings in its own file so one doesn't have to edit each
# task file individually.
# This file can have any name, but the following variable always should be named
# "global_settings".
global_settings = {
    # sound_notification: (bool) (Optional)
    #
    # Possible values: True or False. Default: False
    #
    # Play a sound at the end of each backup job.
    "sound_notification": True,

    # mail_notification: (bool) (Optional)
    #
    # Possible values: True or False. Default: False
    #
    # Send an email at the end of each backup job.
    "mail_notification": False,

    # mail_settings: (str) (Mandatory if "mail_notification" is set to True)
    #
    # All values inside this setting are mandatory except when specified
    # otherwise.
    # In most cases, the settings are self descriptive.
    "mail_settings": {
        "smtp_server": "smtp.gmail.com",
        "smtp_port": 587,
        "sender_address": "user_name@gmail.com",
        "sender_username": "user_name@gmail.com",
        # sender_secret: (tuple)
        #
        # Basically, this tuple will be used to retrieve the email password
        # from the default system's keyring. The password should have been
        # previously stored into the keyring with the following command.
        #
        # $ keyring set backup_secret_context backup_secret_name
        #
        # And then it will be used by this app like so...
        #
        # >>> keyring.get_password("backup_secret_context", "backup_secret_name")
        #
        # Read the Requirements section of this application's documentation for
        # more details.
        "sender_secret": ("backup_secret_context", "backup_secret_name"),
        "use_tls": True,
        # mail_subject: (str) (Optional)
        # It defaults to "Backup Tool Report".
        "mail_subject": "Backup Tool report",
        # mail_body: (str) (Optional)
        "mail_body": "A custom message that will be attached to the default mail body.",
        "mailing_list": [
            "address1@domain.com",
            "address2@domain.com"
        ],
    },
    # ignored_patterns: (list) (Optional)
    #
    # A list of patterns to ignore to be used by all backup jobs.
    "ignored_patterns": [
        "*.ext1",
        "*.ext2"
    ]
}
